/** eslint-disable */
export default class TestController {

    index = (req, res) => {
        Test.find().exec()
            .then(resource => res.send(resource))
            .catch(err => res.status(500).send(err))
    }

    show = (req, res) => {
        Test.findById(req.params.id).exec()
            .then(resource => res.send(resource))
            .catch(err => res.status(500).send(err))
    }

    store = (req, res) => {
        Test.create(req.body)
            .then(resource => res.send(resource))
            .catch(err => res.status(500).send(err))
    } 

    update = (req, res) => {
        Test.findByIdAndUpdate(req.params.id, req.body)
            .then(() => res.send())
            .catch(err => res.status(500).send(err))
    }

}
