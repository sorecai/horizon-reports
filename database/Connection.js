import mongoose from 'mongoose';

export default class Connection {

    init = async () => {
        mongoose.connect(process.env.MONGO_URL, { 
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        .then(() => console.log('Connected to MongoDB'))
        .catch(err => console.log(err));

        mongoose.set('useFindAndModify', false);
    }

}