
import express from 'express';
import bodyParser from 'body-parser';
import compression from 'compression';
import dotenv from 'dotenv';
import helmet from 'helmet';
import cors from 'cors';
import { check } from 'express-validator';

const morgan = require('morgan');

import Connection from './database/Connection';
import { validate } from './utils/validate';
// import TestController from './controllers/TestController';

const app = express()

dotenv.config();

app.use(cors());
app.use(helmet());
app.use(compression());
app.use(express.json());
app.use(bodyParser.json());
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies

// Initialize Connection
// new Connection().init();

// Routes
app.get('/', (req, res) => res.send('Service is Up.'));

// app.get('/test', (req, res) => new TestController().index(req, res));
// app.get('/test', validate([check('username').isEmail()]), (req, res) => new TestController().index(req, res));
// app.get('/test/:id', (req, res) => new TestController().show(req, res));

// Not Found
app.use((req, res) =>
  res.status(404).send({ message: 'Not Found.' })
);

// Service Up
app.listen(process.env.PORT, () => console.log('Server Listening in port: ' + process.env.PORT))