import Connection from '../database/Connection';

class DatabaseSeeder {

    async init() {
        await new Connection().init();

        await this.seed();

        process.exit();
    }

    async seed() {
        // await this.runUsersSeeder();
    }

}

module.exports.init = () => new DatabaseSeeder().init();