FROM node:12-alpine 

ARG BITBUCKET_PASSWORD
ARG BRANCH

# install git
RUN apk add git

WORKDIR /

RUN mkdir horizon-reports

RUN git clone --branch $BRANCH https://rennypoz:$BITBUCKET_PASSWORD@bitbucket.org/bluecubeteam/horizon-reports.git

WORKDIR /horizon-reports

RUN ls -l

RUN npm install

EXPOSE 3000

CMD [ "node", "start.js" ]
